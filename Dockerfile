FROM mcr.microsoft.com/dotnet/core/aspnet:2.1

# ADD ./build-api /build-api
ADD ./build-agent /build-agent

EXPOSE 5000

#WORKDIR $site

ENTRYPOINT ["dotnet", "YaMarket.Api.dll"]
