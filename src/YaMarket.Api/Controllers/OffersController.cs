﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YaMarket.Repository.Models;
using YaMarket.Repository.Services;


namespace YaMarket.Api.Controllers
{
    [Route("saveoffers")]
    public class OffersController : Controller
    {

        public OffersController()
        {

        }

        // GET: api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromQuery] int id, [FromQuery] int location,[FromBody] List<OfferModel> offerModels)
        {
            foreach (var offerModel in offerModels)
            {
               await ProductService.SaveOffer(offerModel);
            }
            await TaskService.Close(new QueueItem()
            {
                id = id,
                location = location,
                parsed = 2,
                updated_date = DateTime.UtcNow,
            });

            return Ok();
        }

      
    }
}
