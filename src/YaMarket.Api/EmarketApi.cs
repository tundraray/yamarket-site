﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Apix.Http.Client;
using YaMarket.Api.Models;

namespace YaMarket.Api
{
    public class EmarketApiClient: HttpClientBase
    {
        public async Task<List<MappingModel>> GetIds(string url, CancellationToken cancellationToken)
        {
            var operationResult = await HttpClient.GetAsync(url,

                requestParameters: new RequestParameters<List<MappingModel>>
                {
                    OnError = CommonBadResponse<List<MappingModel>>
                });

            return operationResult;
        }

        private Task<T> CommonBadResponse<T>(HttpResponseMessage response, CancellationToken cancellationToken)
            where T : class
        {
            switch (response.StatusCode)
            {

                case HttpStatusCode.Conflict:
                case HttpStatusCode.NotFound:
                    return Task.FromResult<T>(null);
                default:
                    DefaultBadResponseAction(response, cancellationToken);
                    break;
            }
            return Task.FromResult<T>(null);
        }

        protected override Task<T> DefaultHandleResponseFunctionAsync<T>(HttpResponseMessage response, CancellationToken cancellationToken)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            return response.Content.ReadAsAsync<T>(new[] { new JsonMediaTypeFormatter() { SupportedMediaTypes = { new MediaTypeHeaderValue("text/html") }, SupportedEncodings = { Encoding.GetEncoding("windows-1251"), Encoding.UTF8 } } }, cancellationToken);
        }
    }
}