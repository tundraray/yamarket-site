﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace YaMarket.Repository.Infrastructure
{
    public static class ListExtension
    {
        public static async Task ForEachAsyncWithExceptions<T>(this IEnumerable<T> source, int dop, Func<T, Task> body)
        {
            ConcurrentQueue<Exception> exceptions = null;
            await Task.WhenAll(
                from partition in Partitioner.Create(source).GetPartitions(dop)
                select Task.Run(async delegate
                {
                    using (partition)
                    {
                        while (partition.MoveNext())
                        {
                            try
                            {
                                await body(partition.Current);
                            }
                            catch (Exception e)
                            {
                                LazyInitializer.EnsureInitialized(ref exceptions).Enqueue(e);
                            }
                        }
                    }
                }));

            if (exceptions != null)
            {
                throw new AggregateException(exceptions);
            }
        }

        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }
    }
}
