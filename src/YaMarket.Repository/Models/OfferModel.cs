﻿using System;
using Apix.Db.Mysql;

namespace YaMarket.Repository.Models
{
    [DatabaseTable("offers")]
    public class OfferModel
    {
        [DatabaseField("market_id",identity:true)]
        public int MarketId { get; set; }

        [DatabaseField("location", identity: true)]
        public int Location { get; set; }

        public int position { get; set; }
        public string shop_name { get; set; }
        public int shop_id { get; set; }
        public double ratings { get; set; }
        public int? price { get; set; }
        public int? delivery_price { get; set; }
        public bool warranty { get; set; }
        public DateTime updated_at { get; set; }
    }
}