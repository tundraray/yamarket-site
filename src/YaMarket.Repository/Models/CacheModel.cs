﻿using Apix.Db.Mysql;

namespace YaMarket.Repository.Models
{
    [DatabaseTable("parsed_data")]
    public class CacheModel
    {
        public int id { get; set; }
        public string data { get; set; }
    }
}