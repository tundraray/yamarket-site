﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YaMarket.Repository.Models
{
    public class QueueItem
    {
        public int id { get; set; }
        public int location { get; set; }
        public DateTime? updated_date { get; set; }
        public byte parsed { get; set; }

        public string slug { get; set; }
    }
}
