﻿using System.Collections.Generic;
using Apix.Db.Mysql;
using Apix.Sync.YaMarket.Models;

namespace YaMarket.Repository.Models
{
    [DatabaseTable("products")]
    public class Product
    {
        public int id { get; set; }
        public int market_id { get; set; }

        [DatabaseField("",ignore:true)]
        public List<OfferModel> Offers { get; set; }
    }
}