﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Apix.Http.Client;
using Microsoft.Extensions.Configuration;
using YaMarket.Repository.Infrastructure;

namespace YaMarket.Repository
{
    public static class AppConfig
    {
        public static IConfiguration Config = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables(prefix: "ASPNETCORE_")
            .Build();
        public static List<string> ApiUrls
        {
            get
            {
                var config = Config.GetSection("MarketSource").Get<List<string>>();
                if (config != null && config.Count > 0)
                {
                    return config;
                }
                else
                {
                    return new List<string>();
                }
            }
        }

        public static List<ProxySettings> Proxies
        {
            get
            {
                var config = Config.GetSection("ProxyConfiguration").Get<ProxyConfiguration>();
                if (config != null && config.UseProxy)
                {
                    return config.Proxies.ToList();
                }
                else
                {
                    return new List<ProxySettings>();
                }
            }
        }

        public static int? Location => Config.GetSection("LOCATION").Get<int?>();
        public static string ApiUrl => Config.GetSection("API_URL").Get<string>();
    }
}
