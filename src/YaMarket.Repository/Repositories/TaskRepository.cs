﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apix.Db.Mysql;
using YaMarket.Repository.Models;

namespace YaMarket.Repository.Repositories
{
    public class TaskRepository : MysqlDataRepositoryBase<QueueItem>
    {
        public TaskRepository(string conn) : base(conn) {}

        public async Task<IEnumerable<QueueItem>> ListActiveTask(string envName = null, int? region = null)
        {

            //@"SELECT mp.* FROM market_products mp where  parsed = 0 or (NOT EXISTS (SELECT 1 FROM offers WHERE market_id = mp.id) and (mp.updated_date + INTERVAL 5 HOUR) <= UTC_TIMESTAMP())");
            var list = await Connection.ExecuteQueryAsync<QueueItem>(
               @"UPDATE market_products mp SET env_name = @envName, `updated_date` = UTC_TIMESTAMP(), parsed = 1 where parsed = 0 and env_name IS NULL  and (@region is null or location = @region) LIMIT 20;
                SELECT mp.*, mi.slug FROM market_products mp 
                inner JOIN market_info mi on mi.marketId = mp.id 
                where parsed = 1 and env_name = @envName and (@region is null or location = @region)", new { envName, region });
            return list.ToList();
           
        }

        internal Task SaveSlug(int id, string slug)
        {
            return ExecuteAsync(@"
                INSERT INTO `market_info` (`marketId`, `slug`) VALUES (@id, @slug);",
                new { id, slug });
        }

        public Task ResetNotCompletedTask(string envName = null, int? region = null)
        {
            return ExecuteAsync(@"
                UPDATE market_products SET `parsed`=0, `updated_date` = null, env_name = null WHERE parsed <> 2 and env_name = @envName and (@region is null or location = @region) and DATE_ADD(updated_date, INTERVAL 1 HOUR)<UTC_TIMESTAMP()",
                new { envName, region });
        }

        public  Task Close(QueueItem queue)
        {
            return ExecuteAsync(@"
                UPDATE market_products SET `parsed`= @parsed, `updated_date` = @date WHERE id = @id and location = @location;",
            new { queue.id, queue.location, date = queue.updated_date, parsed = queue.parsed });
        }

        public Task Reset(QueueItem queue)
        {
            return ExecuteAsync(@"
                UPDATE market_products SET `parsed`=0, `updated_date` = null, env_name = NULL WHERE id = @id and location = @location;",
                new { queue.id, queue.location });
        }

        public Task InsertSoft(int id, int location)
        {
            return ExecuteAsync(@"
                UPDATE market_products SET `parsed`=0, `updated_date` = null, env_name = null WHERE id = @id and location = @location and parsed <> 0 and DATE_ADD(updated_date, INTERVAL 1 HOUR) < UTC_TIMESTAMP();
                INSERT INTO market_products (id,location)
                       SELECT @id,@location FROM DUAL
                       WHERE NOT EXISTS (SELECT 1 FROM market_products WHERE id = @id and location = @location);",
            new { id, location });
        }

        public Task Insert(int id, int location)
        {
            return ExecuteAsync(@"
                UPDATE market_products SET `parsed`=0, `updated_date` = null, env_name = null WHERE id = @id and location = @location  and `parsed` <> 0;
                INSERT INTO market_products (id,location)
                       SELECT @id,@location FROM DUAL
                       WHERE NOT EXISTS (SELECT 1 FROM market_products WHERE id = @id and location = @location);",
                new { id, location });
        }

        public Task ResetAll( int location)
        {
            return ExecuteAsync(@"
                UPDATE market_products SET `parsed`=0, `updated_date` = null, env_name = null WHERE location = @location;",
            new { location });
        }

        public  Task Insert(int id, int location, int productId)
        {
            return ExecuteAsync(@"
                UPDATE market_products SET `parsed`=0, `updated_date` = null, env_name = null WHERE id = @id and location = @location;
                INSERT INTO `products` (`id`, `market_id`)
                       SELECT @productId, @id FROM DUAL
                       WHERE NOT EXISTS (SELECT 1 FROM products WHERE id = @productId and market_id = @id);
                INSERT INTO market_products (id,location)
                       SELECT @id, @location FROM DUAL
                       WHERE NOT EXISTS (SELECT 1 FROM market_products WHERE id = @id);",
            new { id , location, productId });
        }
    }
}
