﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Apix.Db.Mysql;
using Apix.Sync.YaMarket.Models;
using Jil;
using YaMarket.Repository.Models;

namespace YaMarket.Repository.Repositories
{
    public class CacheRepository : MysqlDataRepositoryBase<CacheModel>
    {
        public CacheRepository(string conn) : base(conn){}

        public async Task<List<Offer>> GetInfo(int id)
        {

            var model = await GetByQueryAsync(c => c.id == id);
            if (model == null) return new List<Offer>();
            using (var input = new StringReader(model.data))
            {
                return JSON.Deserialize<List<Offer>>(input);
            }
        }

        public Task SaveInfo(int id, List<Offer> offers)
        {
            var value = new StringBuilder();
            using (var output = new StringWriter(value))
            {
                JSON.SerializeDynamic(offers, output);
            }

            return ExecuteAsync(@"
                UPDATE parsed_data SET `data`=@value WHERE id = @id;
                INSERT INTO parsed_data (id, `data`)
                       SELECT @id, @value
                       WHERE NOT EXISTS (SELECT 1 FROM parsed_data WHERE id = @id);",
            new { id, value = value.ToString() });
        }
    }
}
