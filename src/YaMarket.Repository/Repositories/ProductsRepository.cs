﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Apix.Db.Mysql;
using Dapper;
using MySql.Data.MySqlClient;
using YaMarket.Repository.Models;

namespace YaMarket.Repository.Repositories
{
    public class ProductsRepository : MysqlDataRepositoryBase<Product>
    {
        public ProductsRepository(string conn) : base(conn){}

        public  Task<IEnumerable<Product>> ListProcessed(int location,int offset = 0, int count = 20)
        {
            return Connection.ExecuteQueryAsync<Product>(
                @"SELECT p.* FROM products p INNER JOIN market_products mp ON mp.id = p.market_id and mp.parsed = 1 and mp.location = @location ORDER BY mp.updated_date DESC LIMIT @offset, @count",
                new { location, offset, count});

        }

        public Task<Product> Get(int productId)
        {
            return GetByQueryAsync(q => q.id == productId);
        }


    }
}