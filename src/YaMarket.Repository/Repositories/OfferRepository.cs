﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apix.Db.Mysql;
using Apix.Extensions;
using Apix.Sync.YaMarket.Models;
using Jil;
using YaMarket.Repository.Models;

namespace YaMarket.Repository.Repositories
{
    public class OffersRepository : MysqlDataRepositoryBase<OfferModel>
    {
        public OffersRepository(string conn) : base(conn) { }

        public async Task<List<OfferModel>> ListById(int id, int location)
        {
            var model = await ListByQueryAsync(c => c.MarketId == id && c.Location == location);
            if (model == null) return new List<OfferModel>();
            return model.OrderBy(d => d.position).ToList();
        }

        public async Task<List<OfferModel>> ListByMvideoId(int id, int location)
        {
            var model = await Connection.ExecuteQueryAsync<OfferModel>(
                $"{MySqlGenerator.SelectAllQuery<OfferModel>()} where `shop_name` like '%М.ВИДЕО%' and market_id = @id and location = @location",
                new { location, id });
            if (model == null) return new List<OfferModel>();
            return model.OrderBy(d => d.position).ToList();
        }

        public Task<IEnumerable<OfferModel>> ListLastDiff(int location, DateTime dateTime)
        {
            return ListByQueryAsync(c => c.updated_at > dateTime && c.Location == location);
        }

        public Task<IEnumerable<OfferModel>> ListByLocation(int location)
        {
            return ListByQueryAsync(c =>  c.Location == location);
        }

       

        public Task SaveOffer(OfferModel offer)
        {
            return ExecuteAsync(@"
                DELETE FROM offers WHERE market_id = @id and position = @pos and location = @location;
                INSERT INTO offers (market_id, position,location, shop_name, shop_id, ratings, price, delivery_price, warranty)
                       VALUES( @id, @pos,@location, @shopName, @shopId, @ratings, @price, @deliveryPrice, @warranty)",
                new
                {
                    id = offer.MarketId,
                    pos = offer.position,
                    location = offer.Location,
                    shopName = offer.shop_name,
                    shopId = offer.shop_id,
                    ratings = offer.ratings,
                    price = offer.price,
                    deliveryPrice = offer.delivery_price,
                    warranty = offer.warranty
                });
        }

        public Task DeleteByLocation(int location)
        {
            return ExecuteAsync(@"DELETE FROM offers WHERE location = @location;", new{ location });
        }
    }
}