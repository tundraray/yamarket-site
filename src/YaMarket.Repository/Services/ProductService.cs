﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YaMarket.Repository.Infrastructure;
using YaMarket.Repository.Models;
using YaMarket.Repository.Repositories;

namespace YaMarket.Repository.Services
{
    public static class ProductService
    {
        public static ProductsRepository Repository => new ProductsRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]);
        public static async Task<List<Product>> List(int location, int offset = 0, int count = 20)
        {
            var products = await Repository.ListProcessed(location, offset, count);
            await products.ForEachAsyncWithExceptions(5, async product =>
            {
                product.Offers = await GetOffers(product.market_id, location);
            });

            return products.ToList();
        }
        public static async Task<Product> Get(int productId,int location)
        {
            var product = await Repository.Get(productId);
            if (product == null)
                return null;
            product.Offers = await GetOffers(product.market_id, location);
            return product;
        }

        public static Task<List<OfferModel>> GetOffers(int marketId, int location)
        {
            return new OffersRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]).ListById(marketId, location);
        }

        public static Task<List<OfferModel>> GetOffersMvideo(int marketId, int location)
        {
            return new OffersRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]).ListByMvideoId(marketId, location);
        }

        

        public static Task<IEnumerable<OfferModel>> DiffOffers(int region, DateTime date)
        {
            return new OffersRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]).ListLastDiff(region, date);
        }

        public static Task<IEnumerable<OfferModel>> ListOffers(int region)
        {
            return new OffersRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]).ListByLocation(region);
        }


        public static Task ResetAll(int location)
        {
            return new OffersRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]).DeleteByLocation(location);
        }

        public static Task SaveOffer(OfferModel offer)
        {
            return new OffersRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]).SaveOffer(offer);
        }
    }
}
