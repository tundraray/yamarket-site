﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using YaMarket.Worker;

namespace YaMarket.Agent
{
    public class Program
    {
        public static StaticQueue Queue = StaticQueue.Instance;
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddCommandLine(args)
                .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                .Build();

            Queue.RunJobs(config.GetValue("WORKER_COUNT", 4));
            Console.ReadKey();
        }
    }
}
