﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Apix.Http.Client;
using YaMarket.Repository.Models;

namespace Yamarket.Client
{
    public class ApiClient: HttpClientBase
    {
        private readonly string _httpBase;

        public ApiClient(string httpBase)
        {
            _httpBase = httpBase;
        }

        public Task SaveOffers(int id,int location,List<OfferModel> offers, CancellationToken cancellationToken)
        {
            return HttpClient.PostAsync($"{_httpBase}/saveoffers?id={id}&location={location}", offers);
        }

       
    }
}
